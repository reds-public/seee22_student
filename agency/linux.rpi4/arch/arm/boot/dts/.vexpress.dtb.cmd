cmd_arch/arm/boot/dts/vexpress.dtb := gcc -E -Wp,-MMD,arch/arm/boot/dts/.vexpress.dtb.d.pre.tmp -nostdinc -I./scripts/dtc/include-prefixes -undef -D__DTS__ -x assembler-with-cpp -o arch/arm/boot/dts/.vexpress.dtb.dts.tmp arch/arm/boot/dts/vexpress.dts ; ./scripts/dtc/dtc -O dtb -o arch/arm/boot/dts/vexpress.dtb -b 0 -iarch/arm/boot/dts/ -i./scripts/dtc/include-prefixes -Wno-interrupt_provider -Wno-unit_address_vs_reg -Wno-unit_address_format -Wno-avoid_unnecessary_addr_size -Wno-alias_paths -Wno-graph_child_address -Wno-simple_bus_reg -Wno-unique_unit_address -Wno-pci_device_reg  -d arch/arm/boot/dts/.vexpress.dtb.d.dtc.tmp arch/arm/boot/dts/.vexpress.dtb.dts.tmp ; cat arch/arm/boot/dts/.vexpress.dtb.d.pre.tmp arch/arm/boot/dts/.vexpress.dtb.d.dtc.tmp > arch/arm/boot/dts/.vexpress.dtb.d

source_arch/arm/boot/dts/vexpress.dtb := arch/arm/boot/dts/vexpress.dts

deps_arch/arm/boot/dts/vexpress.dtb := \
  arch/arm/boot/dts/vexpress-v2m-rs1.dtsi \
  arch/arm/boot/dts/soo.dtsi \

arch/arm/boot/dts/vexpress.dtb: $(deps_arch/arm/boot/dts/vexpress.dtb)

$(deps_arch/arm/boot/dts/vexpress.dtb):
