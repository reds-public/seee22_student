cmd_scripts/gcc-plugins/arm_ssp_per_task_plugin.so := g++ -Wp,-MMD,scripts/gcc-plugins/.arm_ssp_per_task_plugin.so.d -Wall -O2   -fPIC -I /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include -I scripts/gcc-plugins -std=gnu++98 -fno-rtti -fno-exceptions -fasynchronous-unwind-tables -ggdb -Wno-narrowing -Wno-unused-variable -Wno-c++11-compat -Wno-format-diag -shared -o scripts/gcc-plugins/arm_ssp_per_task_plugin.so scripts/gcc-plugins/arm_ssp_per_task_plugin.c

source_scripts/gcc-plugins/arm_ssp_per_task_plugin.so := scripts/gcc-plugins/arm_ssp_per_task_plugin.c

deps_scripts/gcc-plugins/arm_ssp_per_task_plugin.so := \
  scripts/gcc-plugins/gcc-common.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/bversion.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/gcc-plugin.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/auto-host.h \
    $(wildcard include/config/sjlj/exceptions.h) \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/ansidecl.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/system.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/safe-ctype.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/filenames.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/hashtab.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/libiberty.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/hwint.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/coretypes.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/machmode.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/insn-modes.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/mode-classes.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/signop.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/wide-int.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/double-int.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/real.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/fixed-value.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/hash-table.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/statistics.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/ggc.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/gtype-desc.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/vec.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/inchash.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/mem-stats-traits.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/hash-traits.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/hash-map-traits.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/mem-stats.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/hash-map.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/hash-set.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/input.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/line-map.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/is-a.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/memory-block.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/backend.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tm.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/options.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/flag-types.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/arm/arm-opts.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/arm/arm-flags.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/arm/arm-cores.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/insn-constants.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/dbxelf.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/elfos.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/gnu-user.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/linux.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/linux-android.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/glibc-stdint.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/arm/elf.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/arm/linux-gas.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/arm/linux-elf.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/arm/bpabi.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/arm/linux-eabi.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/arm/aout.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/vxworks-dummy.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/arm/arm.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/insn-modes.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/vxworks-dummy.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/arm/arm-opts.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/initfini-array.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/insn-flags.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/defaults.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/function.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/bitmap.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/obstack.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/sbitmap.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/basic-block.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/cfg-flags.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/cfg.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/dominance.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/cfghooks.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/predict.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/predict.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/hard-reg-set.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/cfgrtl.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/cfganal.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/lcm.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/cfgbuild.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/cfgcleanup.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/plugin-api.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/ipa-ref.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/alias.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/flags.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-core.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/symtab.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/all-tree.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/c-family/c-common.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/ada/gcc-interface/ada-tree.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/cp/cp-tree.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/java/java-tree.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/objc/objc-tree.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/builtins.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/sync-builtins.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/omp-builtins.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/cilk-builtins.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/gtm-builtins.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/sanitizer.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/cilkplus.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/chkp-builtins.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/internal-fn.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/treestruct.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/fold-const.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-check.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/plugin.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/highlev-plugin-common.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/plugin.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/plugin-version.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/configargs.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/system.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/coretypes.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tm.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/line-map.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/input.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-inline.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/version.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/rtl.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/rtl.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/reg-notes.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/insn-notes.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/genrtl.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tm_p.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/arm/arm-flags.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/arm/arm-protos.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/arm/aarch-common-protos.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/config/linux-protos.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tm-preds.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/flags.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/hard-reg-set.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/output.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/except.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/function.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/toplev.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/expr.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/basic-block.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/intl.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/ggc.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/timevar.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/timevar.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/params.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/params.list \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/hash-map.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/emit-rtl.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/debug.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/target.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/insn-codes.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/target.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/target-hooks-macros.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/target-insns.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/langhooks.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/cfgloop.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/cfgloopmanip.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/cgraph.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/cif-code.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/opts.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-pretty-print.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/pretty-print.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/wide-int-print.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/gimple-pretty-print.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-pretty-print.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/c-family/c-common.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/splay-tree.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/cpplib.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/alias.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/fold-const.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/diagnostic-core.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/bversion.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/diagnostic.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-cfgcleanup.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-ssa-operands.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-into-ssa.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/is-a.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/diagnostic.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/diagnostic-core.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-dump.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/splay-tree.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/dumpfile.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-pass.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/timevar.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/pass_manager.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/pass-instances.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/predict.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/ipa-utils.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/attribs.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/varasm.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/stor-layout.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/internal-fn.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/gimple-expr.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/gimple-fold.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/context.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-ssa-alias.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-ssa.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/stringpool.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-ssanames.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/print-tree.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-eh.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/stmt.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/gimplify.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/gimple.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-ssa-alias.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/gimple-expr.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/gimple.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/gsstruct.def \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-phinodes.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-cfg.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/gimple-iterator.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/gimple-ssa.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/tree-ssa-operands.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/ssa-iterators.h \
  /opt/toolchains/arm-linux-gnueabihf_6.4.1/bin/../lib/gcc/arm-linux-gnueabihf/6.4.1/plugin/include/builtins.h \
  scripts/gcc-plugins/gcc-generate-rtl-pass.h \

scripts/gcc-plugins/arm_ssp_per_task_plugin.so: $(deps_scripts/gcc-plugins/arm_ssp_per_task_plugin.so)

$(deps_scripts/gcc-plugins/arm_ssp_per_task_plugin.so):
