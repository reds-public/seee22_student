/*
 *
 * -- Smart Object Oriented  --
 *
 * Copyright (c) 2016-2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 * 
 * - December 2017: Baptiste Delporte
 *
 */

#ifndef KCONFIG_H
#define KCONFIG_H

#include <generated/autoconf.h>

#endif /* KCONFIG_H */
