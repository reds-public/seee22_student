/*
 *
 * -- Smart Object Oriented  --
 *
 * Copyright (c) 2016-2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - January 2018: Baptiste Delporte
 *
 */

#ifndef RECEIVE_H
#define RECEIVE_H

#include <virtshare/soo.h>

bool ME_processing_receive(void);

#endif /* RECEIVE_H */
