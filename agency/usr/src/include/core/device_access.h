/*
 *
 * -- Smart Object Oriented  --
 *
 * Copyright (c) 2016-2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - January 2018: Baptiste Delporte
 *
 */

#ifndef DEVICE_ACCESS_H
#define DEVICE_ACCESS_H

#define SOO_ME_DIRECTORY			"/ME"

void agency_inject_ME_from_memory(void);
void sig_inject_ME_from_memory(int sig);

void inject_MEs_from_filesystem(char *filename);

void device_access_init(void);

#endif /* DEVICE_ACCESS_H */
