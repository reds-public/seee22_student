#!/bin/bash

echo Deploying all MEs into the third partition...


cd ../agency/filesystem

./mount.sh 2

if [ "$1" != "clean" ]; then
        
        
        cd ../../ME/usr
        ./deploy.sh so3virt

        cd ../target
        ./mkuboot.sh meso3

        cd ../../agency/filesystem
        ME_to_deploy="../../ME/target/meso3.itb"
        sudo cp -rf $ME_to_deploy fs/root/so3virt.itb
        echo "$1 deployed"
fi
./umount.sh
