
#include <heap.h>
#include <mutex.h>
#include <completion.h>
#include <memory.h>

#include <uapi/linux/input.h>

#include <device/device.h>
#include <device/driver.h>

#include <soo/evtchn.h>
#include <soo/vbus.h>
#include <soo/console.h>
#include <soo/debug.h>
#include <soo/gnttab.h>

#include <soo/ring.h>
#include <soo/grant_table.h>
#include <soo/vdevfront.h>

#include <asm/mmu.h>

#define VVEXT_PACKET_SIZE	32

#define VVEXT_NAME		"vvext"
#define VVEXT_PREFIX	"[" VVEXT_NAME "] "

typedef struct {
	char buffer[VVEXT_PACKET_SIZE];
} vvext_request_t;

typedef struct  {
	uint16_t type;
	uint16_t code;
	int value;
} vvext_response_t;

/*
 * Generate blkif ring structures and types.
 */
DEFINE_RING_TYPES(vvext, vvext_request_t, vvext_response_t);

typedef struct {
	vdevfront_t vdevfront;

	vvext_front_ring_t ring;
	grant_ref_t ring_ref;
	grant_handle_t handle;
	uint32_t evtchn;
	uint32_t irq;

} vvext_t;

typedef struct {
	vvext_t vvext;
	completion_t reader_wait;

} vvext_priv_t;

/* At this point, the FE is not connected. */
void vvext_reconfiguring(struct vbus_device *vdev) {
	int res;
 	struct vbus_transaction vbt;
	vvext_priv_t *vvext_priv = dev_get_drvdata(vdev->dev);

		DBG0("[vvext] Frontend reconfiguring\n");

	DBG0("[vvext] Frontend reconfiguring\n");
	/* The shared page already exists */
	/* Re-init */

	gnttab_end_foreign_access_ref(vvext_priv->vvext.ring_ref);

	DBG("Frontend: Setup ring\n");

	/* Prepare to set up the ring. */
	SHARED_RING_INIT(vvext_priv->vvext.ring.sring);
	FRONT_RING_INIT(&vvext_priv->vvext.ring, vvext_priv->vvext.ring.sring, PAGE_SIZE);

	/* Prepare the shared to page to be visible on the other end */

	res = vbus_grant_ring(vdev, phys_to_pfn(virt_to_phys_pt((uint32_t) vvext_priv->vvext.ring.sring)));
	if (res < 0)
		BUG();

	vvext_priv->vvext.ring_ref = res;

	vbus_transaction_start(&vbt);

	vbus_printf(vbt, vdev->nodename, "ring-ref", "%u", vvext_priv->vvext.ring_ref);
	vbus_printf(vbt, vdev->nodename, "ring-evtchn", "%u", vvext_priv->vvext.evtchn);

	vbus_transaction_end(vbt);
}
