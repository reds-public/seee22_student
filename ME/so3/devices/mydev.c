/*
 * Copyright (C) 2016-2018 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/* Simple of example of devclass device */

#include <vfs.h>

#include <device/driver.h>

/* to be completed */

struct file_operations mydev_fops = {
	/* to be completed */
};

struct devclass mydev_dev = {
	.class = "mydev",
	.type = VFS_TYPE_DEV_CHAR,
	.fops = &mydev_fops,
};


int mydev_init(dev_t *dev) {

	/* Register the framebuffer so it can be accessed from user space. */
	devclass_register(dev, &mydev_dev);

	return 0;
}


REGISTER_DRIVER_POSTCORE("arm,mydev", mydev_init);
